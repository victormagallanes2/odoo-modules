# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PartnerCustomField(models.Model):
    _inherit = 'res.partner'

    vat = fields.Char(help="What needs to be done?")