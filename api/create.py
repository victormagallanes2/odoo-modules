import xmlrpc.client

# credenciales
url = "http://localhost:8072"
db = "creditdb9"
username = 'admin'
password = 'admin'

# autenticacion
common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})

# sentencia
models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
new_register = models.execute_kw(db, uid, password, 'res.partner', 'create', [{
    'name': "Jesus Gerardo Laya Rodriguez",
}])