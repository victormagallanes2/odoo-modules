import xmlrpc.client

# credenciales
url = "http://18.234.71.147:8069"
db = "odoo11"
username = 'admin'
password = 'admin'

# autenticacion
common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})

# sentencia
models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
list_user = models.execute_kw(db, uid, password, 'res.partner', 'search', [[]])
print(list_user)