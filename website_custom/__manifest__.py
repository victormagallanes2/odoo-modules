# -*- coding: utf-8 -*-
{
    # Theme information
    'name': "Website Bella",
    'description': """ Website bella by Markdebrand LLC
    """,
    'category': 'Theme',
    'version': '1.0',
    'depends': ['website'],
    # templates
    'data': [
        'views/header.xml',
        'views/footer.xml',
        'views/assets.xml',
        'views/home.xml',
        'views/human_resource.xml',
        'views/tecnology.xml',
        'views/legal.xml',
        'views/operations.xml',
        'views/marketing.xml',
        'views/finance.xml',
        'views/sales.xml',
    ],

    # demo pages
    'demo': [
        #'demo/pages.xml',
    ],

    # Your information
    'author': "victor magallanes",
    'website': "https://bella.markdebrand.com",
}


